provider "google" {
  credentials = file("") # add credential file here
  project     = var.project_name
  region      = var.regiao
}

resource "google_compute_instance" "webserver" {
  name         = var.nome
  machine_type = var.tipo_maquina
  zone         = var.zona

  boot_disk {
    initialize_params {
      image = var.imagem
    }
  }

  # Instala o servidor web Apache
  metadata_startup_script = "sudo apt-get update; sudo apt-get install apache2 -y; echo Testando > /var/www/html/index.html"

  # Habilita rede para a VM bem como um IP público
  network_interface {
    network = "default"
    access_config {

    }
  }
}

resource "google_compute_firewall" "webfirewall" {
  name    = var.nome_fw
  network = "default"

  allow {
    protocol = "tcp"
    ports    = var.portas
  }
}

resource "null_resource" "installSql" {
  connection {
    type        = "ssh"
    user        = "marco"
    private_key = file("~/.ssh/google_compute_engine")
    host        = google_compute_instance.webserver.network_interface.0.access_config.0.nat_ip
  }

  provisioner "file" {
    source      = "install-mysql.sh"
    destination = "./install-mysql.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x install-mysql.sh",
      "sudo bash install-mysql.sh",
    ]

  }


  depends_on = [
    google_compute_instance.webserver
  ]
}
