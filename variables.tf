variable "project_name" {
  type    = string
  default = "terraform-impacta-307901"
}

variable "regiao" {
  type = string
  default = "southamerica-east1"
}

variable "nome" {
  type = string
  default = "vm-webserver"
}

variable "tipo_maquina" {
  type = string
  default = "f1-micro"
}

variable "zona" {
  type = string
  default = "southamerica-east1-a"
}

variable "imagem" {
  type = string
  default = "ubuntu-os-cloud/ubuntu-1604-lts"
}

variable "nome_fw" {
  type = string
  default = "webserver-firewall"
}

variable "portas" {
  type = list
  default = ["80"]
}
