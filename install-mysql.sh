#!/bin/bash

echo "\n --- packages update ---\n"
sudo apt-get -qq update
echo "\n --- packages updated ---\n"

DB_NAME=dbimpacta
DB_USER=dbuser
DB_PASS=password

echo "\n --- mysql user configs ---\n"
debconf-set-selections <<< "mysql-server mysql-server/root_password password $DB_PASS"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DB_PASS"
echo "\n --- mysql user configs done ... ---\n"

echo "\n --- mysql-server install ---\n"
apt-get -y install mysql-server
echo "\n --- mysql-server installed ---\n"

mysql -uroot -p$DB_PASS -e "CREATE DATABASE $DB_NAME"
mysql -uroot -p$DB_PASS -e "grant all privileges on $DB_NAME.* to '$DB_USER'@'%' identified by '$DB_PASS'"

sudo sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

sudo service mysql restart